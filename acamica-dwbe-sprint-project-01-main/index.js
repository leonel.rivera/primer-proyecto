const express = require('express')
const app = express()

app.use(express.json())
   

var usuarios = require('./routes/usuarios');
app.use('/usuarios', usuarios);

var productos = require('./routes/productos');
app.use('/productos', productos);


 
app.listen(3000)