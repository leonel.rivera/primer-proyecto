const moment = require('moment');
const chalk = require('chalk');

//fecha actual
const now = moment(new Date());
//fecha utc
const utc = moment(new Date()).utc();

// Mostramos ambas fechas
console.log(chalk.red(now.format()));
console.log(chalk.redBright(utc.format()));

//Calculamos la diferencia en horas
const duration = moment(utc.diff(now));
const durationInHours = duration.hours();

console.log(`Diferencia de horas ${durationInHours}`);

//Comparar 2 fechas
const actualDate = new Date();
const diff = actualDate.getTimezoneOffset();
const diffInHours = diff/ 60;

diff > 0 ? console.log(`estoy detrás del UTC`) : console.log(`estoy por delante del UTC`);
console.log(chalk.blue(`con una diferencia de ${diffInHours} horas`));
