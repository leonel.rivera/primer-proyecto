


class Alumno {
        constructor(nombre, apellido) {
            this._nombre = nombre;
            this._apellido = apellido;
        }

        get nombre() {
            return this._nombre
        }

        set nombre(valor) {
            this._nombre = valor;
        }

        get apellido() {
            return this._apellido
        }

        set apellido(valor) {
            this._apellido = valor;
        }

        get nombreCompleto() {
            return `${this.nombre} ${this.apellido}`;
        }
    };

let alumno1 = new Alumno('Leonel', 'Rivera');
console.log(alumno1.nombreCompleto);

let alumno2 = new Alumno('Pablo', 'Perez');
console.log(alumno2.nombreCompleto);

let alumnos = ['Leonel','Diego'];

const express = require('express');
const app = express();

app.get('/',function(req,res){
    res.send('HOLA MUNDO');
})

app.get('/alumnos', function (req, res) {
    res.send(`${alumnos}`);
});

app.listen(3000);