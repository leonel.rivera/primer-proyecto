const express = require('express');
const getRandomInt = require('./aleatorio');

const app = express();
const port = 3002;

app.get('/', (req, res) => {
  res.send('hola mundo');
});

app.get('/users', (req, res) => {
  res.send('listado de usuarios');
});

app.post('/users', (req, res) => {
  res.send('usuario agregado correctamente');
});

app.delete('/users', (req, res) => {
  res.send('usuario eliminado correctamente');
});

app.listen(port, () => {
  console.log(`listening on port http://localhost:${port}`);
});


app.get('/pares', (req, res) => {
  const numero = getRandomInt(1,100);
  let mi_respuesta = {msj: numero}

    if(numero % 2 === 0){
      res.status(200);
      mi_respuesta.esPar = true;
    }else{
      res.status(400);
      mi_respuesta.esPar = false;
    }

    res.json(mi_respuesta);
});

