let autores = [
  {
    "id": 1,
    "nombre": "Kelly Carroll",
    "apellido": "Huffman",
    "fechaDeNacimiento": "25/01/2014",
    "libros": [
      {
        "id": 1,
        "titulo": "Cameron Reeves",
        "descripcion": "Exercitation amet est qui sunt mollit aliquip proident laborum. Cillum laboris consequat tempor ad est in velit ipsum do anim. In pariatur nulla proident fugiat sint voluptate anim sit. Exercitation occaecat aliqua reprehenderit consequat officia tempor. Aliquip proident veniam eu commodo mollit consectetur commodo esse consectetur veniam amet. Id commodo Lorem cillum dolore ipsum elit do magna dolore culpa.\r\n",
        "anioPublicacion": 2016
      },
      {
        "id": 2,
        "titulo": "Fuentes Farley",
        "descripcion": "Elit duis ullamco ea minim dolore dolor elit. Sit et duis duis aliqua aliqua nulla quis. Pariatur veniam occaecat dolore pariatur consectetur officia. Veniam ad ipsum est sit id cillum proident quis ullamco. Nulla mollit laborum elit proident veniam ipsum occaecat. Deserunt laboris ex nulla aliquip laborum elit minim mollit sunt ex. Est adipisicing ullamco pariatur proident dolor ullamco labore id commodo occaecat officia.\r\n",
        "anioPublicacion": 2014
      },
      {
        "id": 3,
        "titulo": "Mckay Barr",
        "descripcion": "Incididunt amet deserunt duis anim laborum esse nulla sit amet ea eu. Adipisicing mollit est est anim id occaecat veniam ut minim dolor fugiat sunt. Nulla consequat quis sint officia tempor duis nisi.\r\n",
        "anioPublicacion": 2016
      }
    ]
  },
  {
    "id": 2,
    "nombre": "Karen Humphrey",
    "apellido": "Dean",
    "fechaDeNacimiento": "09/03/2019",
    "libros": [
      {
        "id": 1,
        "titulo": "Glenna Baldwin",
        "descripcion": "Amet et dolore irure amet adipisicing incididunt minim ipsum culpa. Aute sint commodo ex ipsum ad excepteur velit velit. Laboris esse sunt nisi consequat incididunt.\r\n",
        "anioPublicacion": 2014
      },
      {
        "id": 2,
        "titulo": "Chang Holcomb",
        "descripcion": "Incididunt elit excepteur voluptate dolore dolor nostrud commodo. Adipisicing nostrud nulla nisi dolore laborum elit irure sit proident occaecat ipsum duis consectetur reprehenderit. Proident anim et amet cupidatat exercitation tempor ex dolor cupidatat irure ad dolore officia. Dolore ipsum labore minim voluptate. Commodo sunt velit fugiat aute ad reprehenderit. Elit fugiat irure enim nostrud. Consectetur esse deserunt tempor voluptate et dolor qui nulla.\r\n",
        "anioPublicacion": 2019
      },
      {
        "id": 3,
        "titulo": "Reynolds Kirk",
        "descripcion": "Aliqua eu ex voluptate non tempor adipisicing reprehenderit proident. Velit aliquip ex nulla aliquip amet culpa anim sit adipisicing laboris esse ut. Veniam velit aute eiusmod velit nulla excepteur officia aliquip. Velit qui minim labore ullamco exercitation dolore ipsum culpa elit laboris id dolor ut. Aute do fugiat nisi irure fugiat pariatur do sit magna enim est ullamco.\r\n",
        "anioPublicacion": 2018
      }
    ]
  },
  {
    "id": 3,
    "nombre": "Brenda Lamb",
    "apellido": "Benjamin",
    "fechaDeNacimiento": "20/06/2020",
    "libros": [
      {
        "id": 1,
        "titulo": "Wong Aguirre",
        "descripcion": "Sunt elit labore proident ex mollit ullamco culpa enim eiusmod ea. Id aliqua veniam consequat cillum elit elit voluptate laboris ut dolore. Sit exercitation adipisicing nostrud officia tempor reprehenderit ea proident cupidatat laboris. Aliquip excepteur aliqua fugiat magna ad fugiat cillum sunt elit laboris ea et cillum. Irure labore consequat aliquip sint irure.\r\n",
        "anioPublicacion": 2015
      },
      {
        "id": 2,
        "titulo": "Latonya Mcgee",
        "descripcion": "Incididunt proident sunt nulla in enim irure excepteur anim amet voluptate exercitation enim esse. In qui occaecat do quis esse est excepteur laborum deserunt minim pariatur elit ullamco aliquip. Officia magna Lorem est excepteur consectetur sunt cillum fugiat adipisicing adipisicing sunt aliqua incididunt. Ad laborum proident aliquip velit id et ea. Consequat officia et qui minim qui ullamco excepteur et.\r\n",
        "anioPublicacion": 2017
      },
      {
        "id": 3,
        "titulo": "Marva Carr",
        "descripcion": "Ea do magna eu est sit quis dolore dolor minim excepteur duis consectetur est. Duis non dolore laborum sunt officia elit est. Est deserunt fugiat minim qui sint Lorem veniam id adipisicing id do.\r\n",
        "anioPublicacion": 2016
      }
    ]
  },
  {
    "id": 4,
    "nombre": "Trina Barron",
    "apellido": "Mills",
    "fechaDeNacimiento": "23/01/2016",
    "libros": [
      {
        "id": 1,
        "titulo": "Billie Grant",
        "descripcion": "Incididunt Lorem non pariatur voluptate. Amet excepteur voluptate est fugiat duis non esse amet aliqua velit nulla. Nulla aliqua quis anim labore ex magna amet tempor tempor mollit.\r\n",
        "anioPublicacion": 2016
      },
      {
        "id": 2,
        "titulo": "Maureen Sargent",
        "descripcion": "Aliquip minim reprehenderit cupidatat fugiat ea magna et esse aute nulla fugiat ad. Excepteur in deserunt incididunt voluptate in aliqua sunt mollit aliquip nostrud magna labore ea. Exercitation cillum sit voluptate ut cillum eu cillum laboris ex fugiat duis elit enim aliquip. Incididunt incididunt aute deserunt irure. Sunt dolor mollit cupidatat deserunt. Aute dolor nisi enim aute in magna magna in ullamco nisi amet nostrud. Quis eu aliqua aliqua adipisicing laboris laboris irure ex in Lorem.\r\n",
        "anioPublicacion": 2021
      },
      {
        "id": 3,
        "titulo": "Amie Manning",
        "descripcion": "Et enim culpa enim sit in. Eu fugiat dolore enim esse nostrud velit do elit sit nostrud cillum. Dolor minim ea occaecat deserunt veniam ut in fugiat laborum ad ullamco aute esse in. Fugiat voluptate veniam veniam deserunt eiusmod nostrud esse cillum. Tempor culpa occaecat pariatur quis duis sint cillum sunt sit nisi veniam. Amet elit ut nostrud nisi. Culpa consequat et sit irure minim.\r\n",
        "anioPublicacion": 2016
      }
    ]
  },
  {
    "id": 5,
    "nombre": "Pacheco Tucker",
    "apellido": "Mccoy",
    "fechaDeNacimiento": "15/10/2016",
    "libros": [
      {
        "id": 1,
        "titulo": "Hollie Gallegos",
        "descripcion": "Magna nulla sit dolor irure ut aute laboris velit esse magna magna sunt. Id adipisicing pariatur ea excepteur amet labore esse. Laboris ut ex nisi reprehenderit nostrud eiusmod amet aute enim aliquip laboris fugiat consequat consectetur. Cupidatat officia excepteur cillum deserunt veniam reprehenderit laboris Lorem quis consectetur elit dolore. Nisi voluptate nisi enim fugiat sint ad voluptate aliquip adipisicing.\r\n",
        "anioPublicacion": 2014
      },
      {
        "id": 2,
        "titulo": "Gallegos Mccullough",
        "descripcion": "Nostrud et adipisicing tempor dolore aute. Amet eu do pariatur in et ipsum laboris Lorem fugiat enim exercitation aute dolore elit. Id adipisicing ex est minim ea. Sint cillum excepteur sunt fugiat. Anim cupidatat tempor nulla aliquip incididunt id id magna minim consequat esse in.\r\n",
        "anioPublicacion": 2016
      },
      {
        "id": 3,
        "titulo": "Dodson Herman",
        "descripcion": "Lorem consequat commodo adipisicing adipisicing minim eu aute enim cupidatat excepteur Lorem. Non reprehenderit officia enim enim et in. Adipisicing est enim ex incididunt. Irure adipisicing aliquip tempor anim Lorem enim voluptate est duis laborum nulla.\r\n",
        "anioPublicacion": 2019
      }
    ]
  },
  {
    "id": 6,
    "nombre": "Olson Levy",
    "apellido": "Brennan",
    "fechaDeNacimiento": "16/07/2015",
    "libros": [
      {
        "id": 1,
        "titulo": "Hilary Callahan",
        "descripcion": "Mollit dolor reprehenderit deserunt laboris nostrud non. Ad incididunt consequat dolore sint sit ad sunt anim deserunt adipisicing ad. Cupidatat aliqua consequat nulla eu sunt elit anim adipisicing nulla. Irure aute amet voluptate amet ex laboris consequat officia nisi consectetur non amet dolore. Laborum do aliquip cillum in commodo veniam mollit do.\r\n",
        "anioPublicacion": 2015
      },
      {
        "id": 2,
        "titulo": "Powers Mcdaniel",
        "descripcion": "Deserunt deserunt ipsum eiusmod labore culpa mollit laborum ex esse irure quis nulla sit proident. Anim esse sit ex nostrud culpa reprehenderit Lorem aliquip exercitation amet. Voluptate adipisicing est et id ullamco eu exercitation irure sit cupidatat minim id laborum.\r\n",
        "anioPublicacion": 2017
      },
      {
        "id": 3,
        "titulo": "Stafford Puckett",
        "descripcion": "Culpa dolor id anim officia anim consectetur. Adipisicing magna enim nostrud veniam excepteur labore sunt laborum incididunt. Irure id sint reprehenderit occaecat aute mollit pariatur velit minim anim pariatur laboris ipsum enim. Tempor consequat veniam eiusmod nostrud fugiat. Ea proident et dolore nisi nostrud ad consectetur ut dolor nostrud esse exercitation nulla. Consequat ex anim deserunt qui enim qui amet esse qui velit ad. Consequat dolore sit labore veniam ipsum eu eu voluptate do qui enim.\r\n",
        "anioPublicacion": 2017
      }
    ]
  }
];

module.exports = autores;