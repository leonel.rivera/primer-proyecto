const getRandomInt = require('../ejemplo_express/aleatorio');
let autores  = require('./autores');

const idExiste = (id_autor) => {
  const busqueda = autores.filter(autor => autor.id == id_autor);
  return busqueda.length > 0;
}

const buscarIndiceAutor = (id_autor) => {
  return autores.findIndex(x => x.id == id_autor);
}

const buscarIndiceLibro = (autor, id_libro) => {
  return autor.libros.findIndex(x => x.id == id_libro);
}

const mostrarAutores = () => {
  return autores;
}

const crearAutor = (autor) => {
  const id = getRandomInt(1, 10000);
  autores.push({
    id: id,
    nombre: autor.nombre,
    apellido: autor.apellido,
    fechaDeNacimiento: autor.fechaDeNacimiento,
    libros: []
  });
  return 'author created';
}

const actualizarAutor = (id_autor, object) => {
  object = {
    id: parseInt(id_autor),
    nombre: object.nombre,
    apellido: object.apellido,
    fechaDeNacimiento: object.fechaDeNacimiento,
    libros: []
  };
  autores[buscarIndiceAutor(id_autor)] = object;
  return `Author updated`;
}

const borrarAutor = (idAutor) => {
  autores = autores.filter(autor => autor.id != idAutor);
  return `Author deleted`;
}

const buscarAutor = (idAutor) => {
  return autores.filter(autor => autor.id == idAutor);
}

const mostrarLibrosDeAutor = (idAutor) => {

  if(!idExiste(idAutor)){
    return `this author does not exists, check all authors in /autores endPoint`;
  }
  const autor = buscarAutor(idAutor);
  return autor[0].libros.length == 0 ? `this author does not has books` : autor[0].libros;
}

const crearLibrosdeAutor = (idAutor, object) => {

  if(!idExiste(idAutor)){
    return `this author does not exists, check all authors in /autores endPoint`;
  }
  const autor = buscarAutor(idAutor);
  autor[0].libros.push({
    id : getRandomInt(1, 10000),
    titulo: object.titulo,
    descripcion: object.descripcion,
    anioPublicacion: object.anioPublicacion
  });
  return `book created`;
}

const obtenerLibrodeAutor = (idAutor, idLibro) => {
  const autor = buscarAutor(idAutor);
  const libros = autor[0].libros;
  return libros.filter(libro => libro.id == idLibro);

}

const mostrarLibroDeAutor = (idAutor, idLibro) => {
  if(!idExiste(idAutor)){
    return `this author does not exists, check all authors in /autores endPoint`;
  }
  const libro = obtenerLibrodeAutor(idAutor, idLibro);
  return libro.length == 0 ? `this book does not exists, check all books for this author in /autores/${idAutor}/libros endPoint` : libro[0];
}

const actualizarLibro = (idAutor, idLibro, object) => {
  if(!idExiste(idAutor)){
    return `this author does not exists, check all authors in /autores endPoint`;
  }
  const libro = obtenerLibrodeAutor(idAutor, idLibro);
  if(libro.length == 0 ){
    return `this book does not exists, check all books for this author in /autores/${idAutor}/libros endPoint`;
  }
  const indiceLibro = buscarIndiceLibro(autores[buscarIndiceAutor(idAutor)], idLibro);

  autores[buscarIndiceAutor(idAutor)].libros[indiceLibro] = object;
  return `book updated`;
}

const borrarLibro = (idAutor, idLibro) => {
  if(!idExiste(idAutor)){
    return `this author does not exists, check all authors in /autores endPoint`;
  }
  const libro = obtenerLibrodeAutor(idAutor, idLibro);
  if(libro.length == 0 ){
    return `this book does not exists, check all books for this author in /autores/${idAutor}/libros endPoint`;
  }
  const indiceLibro = buscarIndiceLibro(autores[buscarIndiceAutor(idAutor)], idLibro);

  autores[buscarIndiceAutor(idAutor)].libros = autores[buscarIndiceAutor(idAutor)].libros.filter(libro => libro.id != idLibro);
  return `book deleted`;
}

module.exports = {
  idExiste,
  buscarIndiceAutor,
  actualizarAutor,
  borrarAutor,
  mostrarAutores,
  crearAutor,
  buscarAutor,
  mostrarLibrosDeAutor,
  crearLibrosdeAutor,
  obtenerLibrodeAutor,
  mostrarLibroDeAutor,
  actualizarLibro,
  borrarLibro
}