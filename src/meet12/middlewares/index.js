const express = require('express');
const getRandomInt = require('../ejemplo_express/aleatorio');
const app = express();
const port = 3007;

app.use(function(req, res, next) {
  console.log(`la hora en unix es: ${Date.now()}`);
  next();
});

const particular = (req, res, next) => {
  console.log(`método particular`);
  next();
}

const middle_par = (req, res, next) => {
  const numerito =  getRandomInt(1, 100);
  let mi_respuesta = {numero: numerito};

  if(numerito % 2 == 0) {
    req.numero = numerito;
    next();
  }else{
    mi_respuesta.message = "no continuo la ejecución por que el número es impar";
    res.status(404).json(mi_respuesta);
  }

}

app.get('/particular', particular, function(req, res) {
  let mi_respuesta = {msj: 'endPoint particular'};
  res.json(mi_respuesta);
});


app.get('/par', middle_par, function(req, res) {
  let mi_respuesta = {msj: 'el número al azar es par'};
      mi_respuesta.numero = req.numero;
  res.json(mi_respuesta);
});


app.get('/par/:id', function(req, res) {
  let numerito = req.params.id;
  let mi_respuesta = {numero: numerito};
  if(numerito % 2 == 0){
    mi_respuesta.par = true;
    res.status(200);
  }else{
    mi_respuesta.par = false;
    res.status(400);
  }

  res.json(mi_respuesta);

});

app.listen(3007, function() {
  console.log(`listening on port http://localhost:${port}`);
});
