const express = require('express');
const app = express();
const port = 3009;


const UrlLogger = (req, res, next) => {
  console.log(req.url);
  next();
}

app.use(UrlLogger);

//estas líneas es para poder enviar un post en formato Json
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const personaExiste = (persona) => {
  return persona == null || persona.length == 0 ? {msj: "la persona no existe"} : {msj: persona};
}

//devuelve true si existe el id en el array de personas
const idExiste = (persona) => {
  return persona <= personas.length;
}

const personas = [
  {id:1, nombre: "Pepe", email: "pepe@nada.com" },
  {id:2, nombre: "Hugo", email: "hugo@nada.com"} ,
  {id:3, nombre: "Juan", email: "juan@nada.com"}
];

app.get('/personas', function (req, res) {
  res.json(personas);
});

app.get('/personas/:id', (req, res) => {
  const id_persona = req.params.id;
  let mi_respuesta = {};

  /* con un ternario validamos si el id existe en el array, en caso positivo, traemos con un * filter por el ID (también podríamos simplemente seleccionar la persona[id] y si no existe * el id devolvemos el mensaje */
  mi_respuesta.msj = idExiste(id_persona) ? personas.filter(persona => persona.id == id_persona) : "esa persona no existe";

  res.json(mi_respuesta);
});

app.post('/buscar', (req, res) => {
  let personaBuscar = req.body.nombre;
  let personaEncontrada = personas.filter(persona => persona.nombre == personaBuscar);
  res.json(personaExiste(personaEncontrada));
});

app.listen(port, () => {
  console.log(`Server listening on port http://localhost:${port}`);
});
