const express = require('express');
const app = express();
const port = 3008;

let vehiculos = [];

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get('/vehiculos', (req, res) => {
  res.json(vehiculos);
});

app.post('/vehiculos', (req, res) => {
  vehiculos.push(req.body);
  res.json({msj: `vehículo agregado correctamente`});
});

app.listen(port, () => {
  console.log(`Listening on port http://localhost:${port}`);
});