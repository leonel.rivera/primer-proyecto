
let escritores = [
    {
        "id": 1,
        "nombre": "Tran William",
        "apellido": "Ball",
        "fechaDeNacimiento": "24/07/2018",
        "libros": [
            {
                "id": 1,
                "titulo": "Kennedy Britt",
                "descripcion": "Exercitation eiusmod ut commodo est officia anim. Et nostrud quis in cupidatat ea veniam quis est in aute laboris incididunt consectetur est. Veniam ex in aute cupidatat sint ea anim eu consequat quis ex nulla duis. Est occaecat consequat mollit ea adipisicing minim fugiat ea. Cupidatat ex ad aliqua aute anim. Ut ex aute aliqua et sit aliqua consequat aliqua.\r\n",
                "anioPublicacion": 2018
            },
            {
                "id": 2,
                "titulo": "Cortez Le",
                "descripcion": "Labore qui in exercitation consectetur. Laboris id laborum reprehenderit Lorem nisi veniam minim eiusmod do non cillum mollit Lorem. Velit et do quis voluptate proident minim nisi esse elit reprehenderit id. Dolore velit irure pariatur in laborum et occaecat.\r\n",
                "anioPublicacion": 2017
            },
            {
                "id": 3,
                "titulo": "Miller Hayden",
                "descripcion": "Ea mollit fugiat laborum commodo qui elit exercitation proident. Culpa consequat ex consequat veniam tempor quis ipsum. Mollit ullamco occaecat ut deserunt qui tempor nisi quis excepteur proident exercitation. Eiusmod nulla dolor irure ex voluptate quis fugiat enim deserunt eiusmod enim consectetur. Mollit ullamco amet minim ea reprehenderit. Dolor eiusmod id non sint laboris nulla irure ullamco duis sunt exercitation ea eu.\r\n",
                "anioPublicacion": 2018
            }
        ]
    },
    {
        "id": 2,
        "nombre": "Sears Bird",
        "apellido": "Spence",
        "fechaDeNacimiento": "10/12/2020",
        "libros": [
            {
                "id": 1,
                "titulo": "Cox Morse",
                "descripcion": "Minim eiusmod qui ea ipsum minim occaecat. Eiusmod proident proident ullamco est incididunt aliqua eu quis consectetur veniam sit eiusmod. Consectetur magna eiusmod Lorem aliquip irure qui ut commodo cillum nostrud ad Lorem sit laborum.\r\n",
                "anioPublicacion": 2016
            },
            {
                "id": 2,
                "titulo": "Baird Dejesus",
                "descripcion": "Id elit dolore culpa esse cupidatat quis amet qui in pariatur exercitation proident cillum. Ea laboris voluptate sint ut sint incididunt. Et fugiat irure cupidatat in mollit cupidatat non aliqua pariatur tempor Lorem consectetur excepteur. Ut eu irure irure id consequat aute Lorem ad nulla mollit sit. Ad irure sint consequat anim adipisicing et cupidatat in Lorem.\r\n",
                "anioPublicacion": 2016
            },
            {
                "id": 3,
                "titulo": "Charlotte Mcfadden",
                "descripcion": "Ullamco culpa velit do enim elit voluptate nisi reprehenderit nulla ad. Incididunt ad ad esse irure officia irure veniam elit. Qui consectetur mollit tempor irure eiusmod anim cupidatat ex occaecat. Qui consequat qui dolor consectetur esse id commodo. Commodo duis adipisicing do laboris ea duis tempor laboris laborum.\r\n",
                "anioPublicacion": 2014
            }
        ]
    },
    {
        "id": 3,
        "nombre": "Becky Medina",
        "apellido": "Strong",
        "fechaDeNacimiento": "17/01/2020",
        "libros": [
            {
                "id": 1,
                "titulo": "Rivas Young",
                "descripcion": "Nostrud ipsum ut aute velit enim et fugiat ullamco consequat excepteur commodo cillum. Laborum non velit irure esse do sit Lorem tempor. Quis reprehenderit sint laboris ea ipsum cupidatat. Tempor culpa minim nulla est cillum cillum velit consequat deserunt.\r\n",
                "anioPublicacion": 2014
            },
            {
                "id": 2,
                "titulo": "Colleen Kane",
                "descripcion": "Sunt est aute et labore sint aute magna qui nisi excepteur laboris. Dolore ullamco labore aliquip elit dolore. Mollit incididunt veniam mollit magna enim cupidatat cillum mollit labore enim consectetur elit eiusmod. Est sunt esse cupidatat voluptate nostrud ad dolore incididunt excepteur officia laborum duis aliquip. Id aliqua nulla anim anim proident Lorem esse esse in consequat adipisicing magna nulla laboris.\r\n",
                "anioPublicacion": 2014
            },
            {
                "id": 3,
                "titulo": "Hurst Gamble",
                "descripcion": "Eu commodo sint qui est pariatur velit aliquip velit eu do. Dolore dolore do anim commodo. Voluptate minim in deserunt irure nisi nulla veniam ea aute. Dolore anim consequat amet laborum id aute commodo culpa sit commodo. Enim non esse enim dolore nostrud tempor magna dolore deserunt id consequat. Et dolor irure mollit proident reprehenderit magna aliquip esse dolor anim eiusmod reprehenderit.\r\n",
                "anioPublicacion": 2020
            }
        ]
    },
    {
        "id": 4,
        "nombre": "Fuentes Fuentes",
        "apellido": "Ward",
        "fechaDeNacimiento": "30/07/2020",
        "libros": [
            {
                "id": 1,
                "titulo": "Karla Winters",
                "descripcion": "Duis aute occaecat fugiat sunt laborum minim culpa veniam. Ex nostrud eu ex dolore irure tempor dolore cillum proident incididunt id. Labore culpa esse qui nostrud aliquip sit eu pariatur excepteur dolor. In ex occaecat velit enim incididunt. Dolor et proident ad cupidatat incididunt sunt nulla exercitation anim. Duis laboris pariatur non enim velit esse commodo in sint deserunt.\r\n",
                "anioPublicacion": 2021
            },
            {
                "id": 2,
                "titulo": "Sonya Cabrera",
                "descripcion": "Consequat exercitation labore qui amet sit sint sint sint quis est tempor excepteur dolor. Non magna sit dolor cupidatat fugiat adipisicing aute incididunt sint dolor. Minim fugiat magna dolore est culpa non esse eiusmod veniam occaecat. Qui eu fugiat quis do nulla sunt.\r\n",
                "anioPublicacion": 2014
            },
            {
                "id": 3,
                "titulo": "Beatriz Franks",
                "descripcion": "Aliqua irure sunt deserunt et irure sint. Sunt cupidatat quis enim Lorem duis dolore est sint amet Lorem amet. Eu magna est dolor irure nulla fugiat officia ad aliquip dolor sint laboris.\r\n",
                "anioPublicacion": 2019
            }
        ]
    },
    {
        "id": 5,
        "nombre": "Glenda Conner",
        "apellido": "Perry",
        "fechaDeNacimiento": "04/09/2019",
        "libros": [
            {
                "id": 1,
                "titulo": "Robin Blair",
                "descripcion": "Esse ut amet aute velit exercitation minim ea ea do laboris voluptate. Occaecat non laboris id ex id et enim consequat mollit. Consequat est nulla consectetur id. Minim sint nisi commodo ut nostrud laborum ullamco excepteur. Ex commodo exercitation ad ex occaecat ad.\r\n",
                "anioPublicacion": 2020
            },
            {
                "id": 2,
                "titulo": "Clayton Fowler",
                "descripcion": "Esse enim commodo fugiat amet eu officia incididunt eiusmod. Ut ullamco veniam velit officia consectetur nisi nulla voluptate voluptate excepteur consequat. Dolore enim ex consequat veniam ea labore irure proident enim cupidatat. Fugiat quis laborum ad ullamco aliqua reprehenderit enim sit. Veniam irure nulla labore id cupidatat irure labore irure in occaecat consequat ullamco. Commodo in adipisicing voluptate et nostrud dolore cillum fugiat deserunt qui.\r\n",
                "anioPublicacion": 2021
            },
            {
                "id": 3,
                "titulo": "Hinton Torres",
                "descripcion": "Lorem occaecat anim non anim minim aute. Officia eu aute sunt nulla ut. Esse eiusmod et pariatur adipisicing anim.\r\n",
                "anioPublicacion": 2019
            }
        ]
    },
    {
        "id": 6,
        "nombre": "Hester Hinton",
        "apellido": "Riggs",
        "fechaDeNacimiento": "29/12/2019",
        "libros": [
            {
                "id": 1,
                "titulo": "Sutton Ashley",
                "descripcion": "Exercitation consectetur cillum laborum culpa sunt cillum excepteur laboris. Qui aute adipisicing ad velit sunt nisi magna. Deserunt enim mollit Lorem tempor sint.\r\n",
                "anioPublicacion": 2016
            },
            {
                "id": 2,
                "titulo": "Drake Francis",
                "descripcion": "Labore commodo aliquip amet magna culpa excepteur qui veniam mollit. Elit ea elit exercitation sint amet veniam tempor duis commodo in ipsum. Ad dolore dolor cupidatat magna velit. Sit nostrud ipsum minim ut laborum culpa occaecat anim.\r\n",
                "anioPublicacion": 2019
            },
            {
                "id": 3,
                "titulo": "Kathie Bowers",
                "descripcion": "Commodo veniam occaecat cillum amet proident nulla. Qui amet consequat tempor dolore occaecat. Sunt excepteur sint ad aliquip dolore voluptate duis nulla laborum tempor. Ipsum culpa aliquip commodo in in nostrud. Incididunt excepteur amet do cupidatat duis ut officia enim. Esse excepteur laborum sint proident minim cupidatat excepteur deserunt elit esse magna magna sit. Sint velit occaecat enim excepteur cillum excepteur reprehenderit.\r\n",
                "anioPublicacion": 2016
            }
        ]
    },
    {
        "id": 7,
        "nombre": "Mueller Travis",
        "apellido": "Mckinney",
        "fechaDeNacimiento": "01/01/2015",
        "libros": [
            {
                "id": 1,
                "titulo": "Letha Stephenson",
                "descripcion": "Est eiusmod laboris tempor ea proident irure culpa voluptate labore aliqua excepteur quis anim voluptate. Tempor quis id et nostrud sint in ullamco pariatur fugiat esse id cupidatat cillum ad. Ad qui adipisicing fugiat nisi.\r\n",
                "anioPublicacion": 2016
            },
            {
                "id": 2,
                "titulo": "Branch English",
                "descripcion": "Exercitation nulla ea id irure reprehenderit dolore tempor deserunt eu aliquip cillum ad. Fugiat dolor ea ut enim ea aliqua laboris sint. Do tempor in dolor ad. Ipsum voluptate ipsum voluptate quis nisi ex do Lorem deserunt ad commodo dolore eiusmod qui.\r\n",
                "anioPublicacion": 2019
            },
            {
                "id": 3,
                "titulo": "Vicki Carver",
                "descripcion": "Laborum culpa deserunt magna esse id id laboris laborum. Do consequat magna do qui nostrud. Ex ipsum nulla enim quis culpa magna occaecat ea. Magna officia ipsum tempor magna sint ex cillum sunt reprehenderit est mollit et. Consequat exercitation laboris irure dolor excepteur laborum qui tempor laboris consequat do. Cupidatat minim laboris officia commodo et anim commodo cupidatat enim. Aliquip mollit Lorem incididunt nostrud et occaecat enim aliqua magna enim veniam sit consequat.\r\n",
                "anioPublicacion": 2019
            }
        ]
    }
];

module.exports = escritores;