const express = require('express');
//const { route } = require('../../../acamica-dwbe-sprint-project-01-main/routes/productos');
const app = express();
const port = 3009;
const router = express.Router();
const escritores = require('./autores');
const validar_autor = require('./middlewares.js');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Acamica API',
            version: '1.0.0'
        }
    },
    apis: ['./index.js'],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api-docs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerDocs));

//app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get('/', (req, res) => {
    res.send('Home');
});

app.get('/autores', (req, res) => {
    res.json(escritores);
});

app.get('/autores/:id', validar_autor, (req, res) => {
    res.json(escritores[parseInt(req.params.id) - 1]);
});

app.post('/autores', (req, res) => {
    escritores.push(req.body);
    res.json({ msj: `Autor agregado correctamente` });
});

app.put('/autores/:id', validar_autor, (req, res) => {
    escritores[parseInt(req.params.id) - 1].nombre = req.body.nombre;
    escritores[parseInt(req.params.id) - 1].apellido = req.body.apellido;
    escritores[parseInt(req.params.id) - 1].fechaDeNacimiento = req.body.fechaDeNacimiento;
    res.json({ msj: `Autor actualizado correctamente` });
});

app.delete('/autores/:id', validar_autor, (req, res) => {
    escritores.splice(parseInt(req.params.id) - 1, 1);
    res.json({ msj: `Autor eliminado exitosamente` });
});

app.listen(port, () => {
    console.log(`Port http://localhost:${port}`);
});

module.exports = router;