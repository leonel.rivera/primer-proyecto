const escritores = require('./autores');


function validar_autor(req, res, next) {
    //console.log(escritores);
    for (let i = 0; i < escritores.length; i++) {
        if ((parseInt(req.params.id) - 1) === escritores[i].id) {
            next();
        } else {
            return res.status(500).json({ "mensaje": "invalido" });
        }
    }
}


module.exports = validar_autor;