let express = require('express');
var app = express();
var router = express.Router();

/*app.use(function (req, res, next) {
    console.log('Time:', Date.now());
    next();
});*/
/////////////////////////////////Middleware de nivel de aplicación

/*app.use('/user/:id', function (req, res, next) {
    console.log('Request Type:', req.method);
    next();
});*/

/* app.get('/user/:id', function (req, res, next) {
    res.send('USER');
}); */

/*app.use('/user/:id', function (req, res, next) {
    console.log('Request URL:', req.originalUrl);
    next();
}, function (req, res, next) {
    console.log('Request Type:', req.method);
    next();
});*/

/*app.get('/user/:id', function (req, res, next) {
    console.log('ID:', req.params.id);
    next();
}, function (req, res, next) {
    res.send('User Info');
});

// handler for the /user/:id path, which prints the user ID
app.get('/user/:id', function (req, res, next) {
    res.end(req.params.id);
});*/

/* app.get('/user/:id', function (req, res, next) {
    // if the user ID is 0, skip to the next route
    if (req.params.id == 0) next('route');
    // otherwise pass the control to the next middleware function in this stack
    else next(); //
}, function (req, res, next) {
    // render a regular page
    //res.render('regular');
    res.json('regular')
});

// handler for the /user/:id path, which renders a special page
app.get('/user/:id', function (req, res, next) {
    //res.render('special');
    res.json('special')
}); */

/////////////////////////////Middleware de nivel de direccionador

// a middleware function with no mount path. This code is executed for every request to the router
/* router.use(function (req, res, next) {
    console.log('Time:', Date.now());
    next();
}); */

// a middleware sub-stack shows request info for any type of HTTP request to the /user/:id path
/* router.use('/user/:id', function (req, res, next) {
    console.log('Request URL:', req.originalUrl);
    next();
}, function (req, res, next) {
    console.log('Request Type:', req.method);
    next();
}); */

// a middleware sub-stack that handles GET requests to the /user/:id path
/* router.get('/user/:id', function (req, res, next) {
    // if the user ID is 0, skip to the next router
    if (req.params.id == 0) next('route');
    // otherwise pass control to the next middleware function in this stack
    else next(); //
}, function (req, res, next) {
    // render a regular page
    //res.render('regular');
    res.json('regular');
});

// handler for the /user/:id path, which renders a special page
router.get('/user/:id', function (req, res, next) {
    console.log(req.params.id);
    //res.render('special');
    res.json('special');
}); */

// mount the router on the app
app.use('/', router);

//////////////////////////////Middleware de manejo de errores

app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});

app.listen(3005);