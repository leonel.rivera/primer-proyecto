const express = require('express');
const router = express.Router();
const getRandomInt = require('../../ejemplo_express/aleatorio')

let comentarios  = require('../models/comentarios');

/**
 * @swagger
 * /comentarios/:
 *  get:
 *    description: lista todos los comentarios
 *    responses:
 *      200:
 *        description: Success
 */
router.get('/', (req, res) => {
  res.json(comentarios);
});

/**
 * @swagger
 * /comentarios:
 *  post:
 *    description: crea un comentario
 *    parameters:
 *    - name: id
 *      description: Id del comentario
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: topico_id
 *      description: id del topico del comentario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: usuario_id
 *      description: id del usuario del comentario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: comentario
 *      description: comentario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post('/', (req, res) => {
  const nuevoComentario = req.body;
  nuevoComentario.id = getRandomInt(1, 10000);
  comentarios.push(nuevoComentario);
  res.json({msj: `comentario agregado correctamente`});
});


/**
 * @swagger
 * /comentarios/{id}:
 *  delete:
 *    description: elimina un comentario de acuerdo a su id
 *    parameters:
 *    - name: id
 *      description: Id del comentario
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete('/:id', (req, res) => {
  const id_comentario= parseInt(req.params.id);
  comentarios = comentarios.filter(comentario => comentario.id != id_comentario);
  res.json({msj: `comentario eliminado correctamente`});
});

/**
 * @swagger
 * /comentarios:
 *  put:
 *    description: actualiza un comentario
 *    parameters:
 *    - name: id
 *      description: Id del comentario
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: topico_id
 *      description: id del topico del comentario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: usuario_id
 *      description: id del usuario del comentario
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: comentario
 *      description: comentario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put('/', (req, res) => {
  id_comentario = req.body.id;
  const indicecomentario = comentarios.findIndex(x => x.id == id_comentario);

  const object = {
    id: parseInt(req.body.id),
    topico_id: parseInt(req.body.topico_id),
    usuario_id: parseInt(req.body.usuario_id),
    comentario: req.body.comentario
  };
  comentarios[indicecomentario] = object;

  res.json({msj: `comentario actualizado correctamente`});
});

module.exports = router;