const express = require('express');
const router = express.Router();
const getRandomInt = require('../../ejemplo_express/aleatorio')

let usuarios  = require('../models/usuarios');

/**
 * @swagger
 * /usuarios/:
 *  get:
 *    description: lista todos los usuarios
 *    responses:
 *      200:
 *        description: Success
 */
router.get('/', (req, res) => {
  res.json(usuarios);
});

/**
 * @swagger
 * /usuarios:
 *  post:
 *    description: crea un usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: nombre
 *      description: nombre del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: apellido
 *      description: apellido del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *      format: email
 *    responses:
 *      200:
 *        description: Success
 */
router.post('/', (req, res) => {
  const nuevoUser = req.body;
  nuevoUser.id = getRandomInt(1, 10000);
  usuarios.push(nuevoUser);
  res.json({msj: `usuario agregado correctamente`});
});


/**
 * @swagger
 * /usuarios/{id}:
 *  delete:
 *    description: elimina un usuario de acuerdo a su id
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete('/:id', (req, res) => {
  const id_usuario= parseInt(req.params.id);
  usuarios = usuarios.filter(usuario => usuario.id != id_usuario);
  res.json({msj: `usuario eliminado correctamente`});
});

/**
 * @swagger
 * /usuarios:
 *  put:
 *    description: actualiza un usuario
 *    parameters:
 *    - name: id
 *      description: Id del usuario
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: nombre
 *      description: nombre del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: apellido
 *      description: apellido del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *      format: email
 *    responses:
 *      200:
 *        description: Success
 */
router.put('/', (req, res) => {
  id_usuario = req.body.id;
  const indiceusuario = usuarios.findIndex(x => x.id == id_usuario);

  const object = {
    id: parseInt(req.body.id),
    nombre: req.body.nombre,
    apellido: req.body.apellido,
    email: req.body.email
  };
  usuarios[indiceusuario] = object;

  res.json({msj: `usuario actualizado correctamente`});
});

module.exports = router;