const express = require('express');
const app = express();
const users = require('./routes/users');
const topics = require('./routes/topics');
const comments = require('./routes/comments');
const host = 'http://localhost:';
const port = 3020;

const swaggerUI = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'API de manejo de Foro',
            version: '1.0.0'
        }
    },
    apis: ['./index.js','./routes/users.js','./models/users.js','./routes/topics.js','./models/topics.js','./routes/comments.js','./models/comments.js'],
};

const swaggerDocs = swaggerJSDoc(swaggerOptions);

app.use('/api-docs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerDocs));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use('/users', users);
app.use('/topics', topics);
app.use('/comments', comments);


app.listen(port, () => {
    console.log(`Listening ${host}${port}`);
});