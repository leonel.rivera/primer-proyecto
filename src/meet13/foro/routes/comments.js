const express = require('express');
const router = express.Router();
let comments = require('../models/comments');

/**
 * @swagger
 * /comments:
 *  get:
 *    description: comments list
 *    responses:
 *      200:
 *        description: Success
 */
router.get('/', (req, res) => {
    console.log(comments);
    res.json({'comments': comments});
});

/**
 * @swagger
 * /comments:
 *  post:
 *    description: create comments
 *    parameters:
 *    - name: id
 *      description: Id comments
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: id_topic
 *      description: Id topics
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: id_user
 *      description: Id users
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: comment
 *      description: Description comments
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post('/', (req, res) => {
    const id_comments = comments.length + 1;
    const newcomments = req.body;
    newcomments.id = id_comments;
    comments.push(newcomments);
    res.json(`Se agrego correctamente comments`);
});

/**
 * @swagger
 * /comments:
 *  put:
 *    description: modify comments
 *    parameters:
 *    - name: id
 *      description: Id comments
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: id_topic
 *      description: Id topics
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: id_user
 *      description: Id users
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: comment
 *      description: Description comments
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put('/', (req, res) => {
    id_comments = req.body.id;
    const indicecomments = comments.findIndex(comments => comments.id == id_comments);

    const object = {
        id: parseInt(req.body.id),
        id_topic: req.body.id_topic,
        id_user: req.body.id_user,
        comment: req.body.comment
    };
    comments[indicecomments] = object;

    res.json(`Se actualizo correctamente comments`);
});

/**
 * @swagger
 * /comments:
 *  delete:
 *    description: delete comments by id
 *    parameters:
 *    - name: id
 *      description: Id comments
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete('/', (req, res) => {
    let id_comments = parseInt(req.body.id);
    //console.log(id_comments);
    comments = comments.filter(comments => comments.id != id_comments);
    console.log(comments);
    //comments.splice((id_comments-1),1);
    res.json(`Se elimino correctamente comments`);
});


module.exports = router;