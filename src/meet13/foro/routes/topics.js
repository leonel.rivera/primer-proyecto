const express = require('express');
const router = express.Router();
let topics = require('../models/topics');

/**
 * @swagger
 * /topics:
 *  get:
 *    description: topics list
 *    responses:
 *      200:
 *        description: Success
 */
router.get('/', (req, res) => {
    console.log(topics);
    res.json({ 'topics': topics });
});

/**
 * @swagger
 * /topics:
 *  post:
 *    description: create topics
 *    parameters:
 *    - name: id
 *      description: Id topics
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: title
 *      description: Title topics
 *      in: formData
 *      required: true
 *      type: string
 *    - name: description
 *      description: Description topics
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post('/', (req, res) => {
    const id_topics = topics.length + 1;
    const newtopics = req.body;
    newtopics.id = id_topics;
    topics.push(newtopics);
    res.json(`Se agrego correctamente topics: ${topics[id_topics-1].title}`);
});

/**
 * @swagger
 * /topics:
 *  put:
 *    description: modify topics
 *    parameters:
 *    - name: id
 *      description: Id topics
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: title
 *      description: Title topics
 *      in: formData
 *      required: true
 *      type: string
 *    - name: description
 *      description: Description topics
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put('/', (req, res) => {
    id_topics = req.body.id;
    const indicetopics = topics.findIndex(topics => topics.id == id_topics);

    const object = {
        id: parseInt(req.body.id),
        title: req.body.title,
        description: req.body.description
    };
    topics[indicetopics] = object;

    res.json(`Se actualizo correctamente topics: ${topics[id_topics-1].title}`);
});

/**
 * @swagger
 * /topics:
 *  delete:
 *    description: delete topics by id
 *    parameters:
 *    - name: id
 *      description: Id topics
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete('/', (req, res) => {
    let id_topics = parseInt(req.body.id);
    //console.log(id_topics);
    topics = topics.filter(topics => topics.id != id_topics);
    console.log(topics);
    //topics.splice((id_topics-1),1);
    res.json(`Se elimino correctamente topics`);
});


module.exports = router;