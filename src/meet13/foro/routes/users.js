const express = require('express');
const router = express.Router();
let users = require('../models/users');

/**
 * @swagger
 * /users:
 *  get:
 *    description: lista todos los users
 *    responses:
 *      200:
 *        description: Success
 */
router.get('/', (req, res) => {
    console.log(users);
    res.json({ 'users': users });
});

/**
 * @swagger
 * /users:
 *  post:
 *    description: create user
 *    parameters:
 *    - name: id
 *      description: Id user
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: name
 *      description: Nmae user
 *      in: formData
 *      required: true
 *      type: string
 *    - name: lastname
 *      description: Lastname user
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Email user
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post('/', (req, res) => {
    const id_user = users.length + 1;
    const newUser = req.body;
    newUser.id = id_user;
    users.push(newUser);
    res.json(`Se agrego correctamente usuario: ${users[id_user-1].name} ${users[id_user-1].lastname}`);
});

/**
 * @swagger
 * /users:
 *  put:
 *    description: modify user
 *    parameters:
 *    - name: id
 *      description: Id user
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: name
 *      description: Name user
 *      in: formData
 *      required: true
 *      type: string
 *    - name: lastname
 *      description: Lastname user
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Email user
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put('/', (req, res) => {
    id_user = req.body.id;
    const indiceUser = users.findIndex(user => user.id == id_user);

    const object = {
        id: parseInt(req.body.id),
        name: req.body.name,
        lastname: req.body.lastname,
        email: req.body.email,
    };
    users[indiceUser] = object;

    res.json(`Se actualizo correctamente usuario: ${users[id_user-1].name} ${users[id_user-1].lastname}`);
});

/**
 * @swagger
 * /users:
 *  delete:
 *    description: delete user by id
 *    parameters:
 *    - name: id
 *      description: Id user
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete('/', (req, res) => {
    let id_user = parseInt(req.body.id);
    //console.log(id_user);
    users = users.filter(user => user.id != id_user);
    console.log(users);
    //users.splice((id_user-1),1);
    res.json(`Se elimino correctamente usuario`);
});


module.exports = router;