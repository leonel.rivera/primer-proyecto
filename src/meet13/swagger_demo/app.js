//esta app usar swagger con el archivo swagger.json de la raíz del proyecto

const express = require('express');
const app = express();
const port = 3012;
const students = require('./students');
app.use('/students', students);


// swagger API
const swaggerUI = require('swagger-ui-express');
const swaggerDocument = require('../../../swagger.json');

app.use('/api-docs',
  swaggerUI.serve,
  swaggerUI.setup(swaggerDocument, {  explorer: true})
  );

app.listen(port, () => {
  console.log(`Example app listening on http://localhost:${port} !`);
});
