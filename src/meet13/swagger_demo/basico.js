// esta app usa swgger con un json externo (petStore)

const express = require('express');
const app = express();
const port = 3015;

const swaggerUi = require('swagger-ui-express');

const options = {
  swaggerOptions: {
    info: {
      title: 'Swagger Petstore',
      description: 'A sample API that uses a petstore as an example to demonstrate features in the swagger-2.0 specification',
      version: '1.0.0'
    },
    url: 'http://petstore.swagger.io/v2/swagger.json'
  }
}

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(null, options));

app.listen(port, () => {
  console.log(`Example app listening on http://localhost:${port} !`);
});