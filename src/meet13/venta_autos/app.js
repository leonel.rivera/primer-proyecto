const express = require('express');
const app = express();
const port = 3017;
const swaggerUI = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const getRandomInt = require('../ejemplo_express/aleatorio')

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'API de Vehículos en venta',
      version: '1.0.0'
    }
  },
  apis: ['./app.js'],
};

const swaggerDocs = swaggerJSDoc(swaggerOptions);



let vehiculos = [
  {id: 1, marca: 'Fiat', modelo: 'Uno', anio: 1999, puertas: 4, disponible: true},
  {id: 2, marca: 'volkswagen', modelo: 'gol', anio: 1999, puertas: 5, disponible: false},
  {id: 3, marca: 'Chevrolet', modelo: 'spark GT', anio: 2001, puertas: 5, disponible: true}
]

app.use(express.urlencoded({ extended: true }));
app.use(express.json());


app.use('/api-docs',
  swaggerUI.serve,
  swaggerUI.setup(swaggerDocs));

/**
 * @swagger
 * /vehiculos:
 *  get:
 *    description: lista todos los vehiculos
 *    responses:
 *      200:
 *        description: Success
 */
app.get('/vehiculos', (req, res) => {
  res.json(vehiculos);
});


/**
 * @swagger
 * /vehiculos:
 *  post:
 *    description: crea un vehiculo
 *    parameters:
 *    - name: id
 *      description: Id del vehiculo
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: marca
 *      description: Marca del vehiculo
 *      in: formData
 *      required: true
 *      type: string
 *    - name: modelo
 *      description: Modelo del vehiculo
 *      in: formData
 *      required: true
 *      type: string
 *    - name: anio
 *      description: Anio del vehiculo
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: puertas
 *      description: Numero de puertas del vehiculo
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: disponible
 *      description: define si el vehículo está disponible para la venta o no
 *      in: formData
 *      required: true
 *      type: boolean
 *    responses:
 *      200:
 *        description: Success
 */
app.post('/vehiculos', (req, res) => {
  const nuevoVehiculo = req.body;
  nuevoVehiculo.id = getRandomInt(1, 10000);
  vehiculos.push(nuevoVehiculo);
  res.json({msj: `vehículo agregado correctamente`});
});


/**
 * @swagger
 * /vehiculos/{id}:
 *  delete:
 *    description: elimina un vehiculo de acuerdo a su id
 *    parameters:
 *    - name: id
 *      description: Id del vehiculo
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
app.delete('/vehiculos/:id', (req, res) => {
  const id_vehiculo= parseInt(req.params.id);
  vehiculos = vehiculos.filter(vehiculo => vehiculo.id != id_vehiculo);
  res.json({msj: `vehículo eliminado correctamente`});
});

/**
 * @swagger
 * /vehiculos:
 *  put:
 *    description: actualiza un vehiculo
 *    parameters:
 *    - name: id
 *      description: Id del vehiculo
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: marca
 *      description: Marca del vehiculo
 *      in: formData
 *      required: true
 *      type: string
 *    - name: modelo
 *      description: Modelo del vehiculo
 *      in: formData
 *      required: true
 *      type: string
 *    - name: anio
 *      description: Anio del vehiculo
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: puertas
 *      description: Numero de puertas del vehiculo
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: disponible
 *      description: define si el vehículo está disponible para la venta o no
 *      in: formData
 *      required: true
 *      type: boolean
 *    responses:
 *      200:
 *        description: Success
 */
app.put('/vehiculos', (req, res) => {
  id_vehiculo = req.body.id;
  const indiceVehiculo = vehiculos.findIndex(x => x.id == id_vehiculo);

  const object = {
    id: parseInt(req.body.id),
    marca: req.body.marca,
    modelo: req.body.modelo,
    anio: parseInt(req.body.anio),
    puertas: parseInt(req.body.puertas),
    disponible: req.body.disponible
  };
  vehiculos[indiceVehiculo] = object;

  res.json({msj: `vehículo actualizado correctamente`});
});


app.listen(port, () => {
  console.log(`Listening on port http://localhost:${port}`);
});