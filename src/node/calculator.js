const chalk = require("chalk");

function Sumar(n1, n2) {
    let resultado;
    resultado = n1 + n2;
    return console.log(chalk.blue(`La operacion ${n1} + ${n2} es igual: ${resultado}`));
}

function Resta(n1, n2) {
    resultado = n1 - n2;
    return console.log(`La operacion ${n1} - ${n2} es igual: ${resultado}`);
}

function Multiplica(n1, n2) {
    resultado = n1 * n2;
    return console.log(`La operacion ${n1} * ${n2} es igual: ${resultado}`);
}

function Divide(n1, n2) {
    if (n2 == 0) {
        console.log('No se puede realizar la operacion');
    } else {
        resultado = n1 / n2;
        return console.log(`La operacion ${n1} / ${n2} es igual: ${resultado}`);
    }
}

exports.Sumar = Sumar;
exports.Resta = Resta;
exports.Multiplica = Multiplica;
exports.Divide = Divide;